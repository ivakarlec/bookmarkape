import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormGroup } from '@angular/forms';
import { By } from '@angular/platform-browser';

import { BookmarkFormComponent } from './bookmark-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BookmarkService } from '../bookmark-service';
import { Bookmark } from '../bookmark';

describe('BookmarkFormComponent', () => {
  let component: BookmarkFormComponent;
  let fixture: ComponentFixture<BookmarkFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [BookmarkService],
      imports: [ReactiveFormsModule, HttpClientTestingModule],
      declarations: [ BookmarkFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarkFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`hides the form initially`, async(() => {
    expect(component.displayForm).toEqual(false);
    expect(fixture.debugElement.query(By.css('form'))).toEqual(null);
  }));

  it('should create a `FormGroup` comprised of `FormControl`s', () => {
      component.ngOnInit();
      expect(component.bookmarkForm instanceof FormGroup).toBe(true);
  });

  it('emits a notify event while submiting the form', () => {
    component.bookmarkForm.controls['name'].setValue('test');
    component.onSubmit();
    component.notify.subscribe( (value) => {  expect(value.name).toBe('test'); });
  });
});
