import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { Bookmark } from '../bookmark';
import { BookmarkService } from '../bookmark-service';

@Component({
  selector: 'app-bookmark-form',
  templateUrl: './bookmark-form.component.html',
  styleUrls: ['./bookmark-form.component.css']
})
export class BookmarkFormComponent implements OnInit {
  @Output() notify: EventEmitter<Bookmark> = new EventEmitter<Bookmark>();
  displayForm: Boolean = false;
  bookmarkForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private bookmarkService: BookmarkService) {
  }

  ngOnInit() {
    this.createForm();
  }

  onSubmit(): void {
    const newBookmark = this.bookmarkForm.value;
    newBookmark.date = new Date().toISOString();
    this.bookmarkService.saveBookmarks(newBookmark)
    .subscribe((response) => {
      this.notify.emit(response);
      this.toggleForm();
    });
  }

  toggleForm() {
    this.displayForm = !this.displayForm;
    if (!this.displayForm) {
      this.bookmarkForm.reset();
    }
  }

  createForm() {
    this.bookmarkForm = this.fb.group({
        name: '',
        description: '',
        link: '',
        date: '',
        tags: '',
        visibility: true
    });
  }

}
