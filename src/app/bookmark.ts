export class Bookmark {
  public name: string;
  public description: string;
  public link: string;
  public date: string;
  public tags: string;
  public visibility: boolean;
}
