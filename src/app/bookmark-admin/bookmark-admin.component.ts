import { Component, OnInit } from '@angular/core';

import { Bookmark } from '../bookmark';
import { BookmarkService } from '../bookmark-service';

@Component({
  selector: 'app-bookmark-admin',
  templateUrl: './bookmark-admin.component.html',
  styleUrls: ['./bookmark-admin.component.css']
})

export class BookmarkAdminComponent implements OnInit {
  bookmarks: Bookmark [] = [];
  isLoading: Boolean = false;

  constructor(private bookmarkService: BookmarkService) { }

  ngOnInit( ) {
    this.getBookmarks();
  }

  getBookmarks() {
    this.isLoading = true;
    this.bookmarkService.fetchBookmarks()
    .subscribe(bookmarks => {
      console.log(bookmarks);
      this.bookmarks = bookmarks;
    });
    this.isLoading = false;
  }
}
