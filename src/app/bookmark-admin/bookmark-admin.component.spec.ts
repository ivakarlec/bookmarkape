import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { BookmarkAdminComponent } from './bookmark-admin.component';
import { BookmarkService } from '../bookmark-service';
import { HttpClient } from '@angular/common/http';

describe('BookmarkAdminComponent', () => {
  let component: BookmarkAdminComponent;
  let fixture: ComponentFixture<BookmarkAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BookmarkService],
      declarations: [ BookmarkAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarkAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
