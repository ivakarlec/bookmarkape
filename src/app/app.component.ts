import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'bookmark ape';
  selected = 'Home';
  navigation = [
    {
      name: 'Home',
      link: '/'
    },
    {
      name: 'Admin',
      link: '/admin'
    }
  ];
  select(item: string) {
    this.selected = item;
  }

  isActive(item: string) {
      return this.selected === item;
  }
}
