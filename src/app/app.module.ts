import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';



import { AppComponent } from './app.component';
import { BookmarkService } from './bookmark-service';
import { BookmarkListComponent } from './bookmark-list/bookmark-list.component';
import { AppRoutingModule } from './app-routing.module';
import { BookmarkAdminComponent } from './bookmark-admin/bookmark-admin.component';
import { BookmarkFormComponent } from './bookmark-form/bookmark-form.component';


@NgModule({
  declarations: [
    AppComponent,
    BookmarkListComponent,
    BookmarkAdminComponent,
    BookmarkFormComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [ BookmarkService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
