import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';

import { Bookmark } from './bookmark';
import { BOOKMARKS } from './mock-bookmarks';

const LOCAL_STORAGE_KEY = 'bookmarkape';

@Injectable()
export class BookmarkService {

  private delayMs = 100;
  private apiHost = 'http://localhost:3000';
  private getBookmarksUrl = `${this.apiHost}/api/allBookmarks`;
  private saveBookmarkUrl = `${this.apiHost}/api/saveBookmark`;

  constructor(private http: HttpClient) {}

  fetchBookmarks(): Observable<Bookmark[]> {
    return this.http.get<Bookmark[]>(this.getBookmarksUrl)
      .pipe(
        catchError(this.handleError('fetchBookmarks', []))
      );
  }

  saveBookmarks(bookmark: Bookmark): Observable<Bookmark> {
    return this.http.put<Bookmark>(this.saveBookmarkUrl, bookmark)
    .pipe(
      catchError(this.handleError('saveBookmark', bookmark))
    );
  }

  getFromLocalStorage(): Bookmark[] {
    let bookmarks: Bookmark[] = BOOKMARKS;
    try {
      bookmarks = JSON.parse(localStorage[LOCAL_STORAGE_KEY] || null) || bookmarks;
    } catch (error) {
      console.error('failed on retrieving data from local storage\n' + error);
    }
    return bookmarks;
  }


  updateLocalStorage(bookmark: Bookmark): Observable<Bookmark> {
    const savedBookmarks = this.getFromLocalStorage();
    savedBookmarks.push(bookmark);
    try {
      localStorage[LOCAL_STORAGE_KEY] = JSON.stringify(savedBookmarks);
      return of(bookmark).delay(this.delayMs);
    } catch (error) {
      console.error('failed on saving data from local storage\n' + error);
    }
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
