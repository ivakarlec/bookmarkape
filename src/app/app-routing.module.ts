import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BookmarkAdminComponent } from './bookmark-admin/bookmark-admin.component';
import { BookmarkListComponent } from './bookmark-list/bookmark-list.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: BookmarkListComponent },
  { path: 'admin', component: BookmarkAdminComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [RouterModule]
})

export class AppRoutingModule { }
