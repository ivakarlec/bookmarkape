import { Component, OnInit } from '@angular/core';

import { Bookmark } from '../bookmark';
import { BookmarkService } from '../bookmark-service';
import { BookmarkFormComponent } from '../bookmark-form/bookmark-form.component';

const ASC = 'asc';
const DESC = 'desc';

@Component({
  selector: 'app-bookmark-list',
  templateUrl: './bookmark-list.component.html',
  styleUrls: ['./bookmark-list.component.css']
})

export class BookmarkListComponent implements OnInit {

  bookmarks: Bookmark [] = [];
  filterTags: string [] = [];
  isLoading: Boolean = false;

  constructor(
    private bookmarkService: BookmarkService) {
  }

  ngOnInit() { this.getBookmarks(); }

  getBookmarks(): void {
    this.isLoading = true;
    this.bookmarkService.fetchBookmarks()
    .subscribe(bookmarks => {
      this.bookmarks = bookmarks;
      this.sortBy('date', DESC);
    });
    this.isLoading = false;
  }

  onNotify(newBookmark: Bookmark): void {
    this.bookmarks.push(newBookmark);
    this.sortBy('date', DESC);
  }

  setFilter(userInput: string) {
    this.filterTags = this.tokenize(userInput);
  }

  applyFilter(tagString: string) {
    if ( this.filterTags.length === 0 ) {
      return true;
    } else {
      const tags: string [] = this.tokenize(tagString);
      for (let i = 0; i < this.filterTags.length; i++) {
        for (let j = 0; j < tags.length; j++ ) {
          if (tags[j].includes(this.filterTags[i])) {
            return true;
          }
        }
        return false;
      }
    }
  }

  tokenize(string: string) {
    return string.toLowerCase().split(',')
    .map(tag => tag.trim())
    .filter(tag => tag );
  }

  sortBy(prop: string, order: string) {
    this.bookmarks = this.bookmarks.sort( (left, right) => {
      if (left[prop] > right[prop]) {
        return order === ASC ? 1 : -1;
      }
      if (left[prop] < right[prop]) {
        return order === ASC ?  -1 : 1;
      }
      return 0;
    });
  }
}
