import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { BookmarkListComponent } from './bookmark-list.component';
import { BookmarkFormComponent } from '../bookmark-form/bookmark-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BookmarkService } from '../bookmark-service';

import { Bookmark } from '../bookmark';
import { BOOKMARKS } from '../mock-bookmarks';

describe('BookmarkListComponent', () => {
  let component: BookmarkListComponent;
  let fixture: ComponentFixture<BookmarkListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [BookmarkService],
      imports: [ReactiveFormsModule, HttpClientTestingModule],
      declarations: [ BookmarkListComponent, BookmarkFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarkListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('sorts bookmarks in descending order by date descending and ascending', () => {
    component.bookmarks = BOOKMARKS;

    component.sortBy('date', 'desc');
    expect(new Date(component.bookmarks[0]['date']).valueOf())
    .toBeGreaterThanOrEqual(new Date(component.bookmarks[1]['date']).valueOf());

    component.sortBy('date', 'asc');
    expect(new Date(component.bookmarks[0]['date']).valueOf())
    .toBeLessThanOrEqual(new Date(component.bookmarks[1]['date']).valueOf());
  });

  it('should tokenize the tags', () => {
    const token = component.tokenize('   TeSt,  ,  angular,    ,   ');
    expect(token).toEqual(['test', 'angular']);
  });

  it('should filter the bookmarks', () => {
    component.setFilter('tes');
    expect(component.applyFilter('test, tag')).toBeTruthy();
    expect(component.applyFilter('angular, tutorial')).toBeFalsy();
  });
});
