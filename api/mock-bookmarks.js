module.exports = [
  {
    name: 'Rosenheimkochs',
    link: 'www.koecheverein-rosenheim.de',
    description: 'My contao page',
    visibility: true,
    date: '2018-01-21T10:00:36.464Z',
    tags: 'Verein, Kochen, Essen'
  },
  {
    name: 'Angular Wiki',
    link: 'https://github.com/angular/angular.js/wiki',
    description: 'Documentation for Angular',
    visibility: true,
    date: '2018-01-23T14:54:36.464Z',
    tags: 'Technologie, IT, Webdevelopent, Wiki, Dokumentation, Angular'
  },
  {
    name: 'Ponozky od babicky',
    link: 'http://elpida.cz/obchod/',
    description: 'Koupit rucne pletene ponozky',
    visibility: true,
    date: '2018-01-18T13:50:16.464Z',
    tags: 'Socken, Kaufen, Senioren, Hilfe'
  }
];
