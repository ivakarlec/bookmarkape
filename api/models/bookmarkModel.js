const mongoose = require('mongoose');

const bookmarkSchema = new mongoose.Schema({
  "name": "String",
  "description" : "String",
  "link" : "String",
  "date": "String",
  "tags": "String",
  "visibility": "Boolean"
});

const Bookmark =  mongoose.model("Bookmark", bookmarkSchema);

module.exports = Bookmark;