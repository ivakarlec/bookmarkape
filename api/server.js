const restify = require('restify');
const corsMiddleware = require('restify-cors-middleware');
const mongoose = require('mongoose');

const config = require('./config');
const server = restify.createServer();

const bookmarkController = require('./controllers/bookmarkController');

const port = 3000;

const cors = corsMiddleware({
  origins: ['http://localhost:4200']
});

server.pre(cors.preflight);
server.use(cors.actual);

server.use(restify.plugins.bodyParser({
  mapParams: true
}));

mongoose.connect(config.getDBConnection());
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("MongoDB connected");
});

bookmarkController(server);

server.listen(port, () => console.info(`Server is up on port ${port}`));