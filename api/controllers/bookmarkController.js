const Bookmark = require('../models/bookmarkModel');
const exampleBookmarks = require('../mock-bookmarks');

updateBookmark = (bookmark, options) => {
  return Bookmark.findOneAndUpdate({name: bookmark.name}, bookmark, options);
}

module.exports = (app) => {

  app.get('api/initialSetup', (req, res) => {
    Promise.all(exampleBookmarks.map(bookmarkElem => {
      return Bookmark.findOneAndUpdate({name: bookmarkElem.name}, bookmarkElem, {new: true, upsert: true});
    })).then( result => {
      res.json({
        status: "SUCCES",
        result: result
      });
    }).catch( err => {
      res.json({
        status: "ERROR",
        message: `Failed loading initial data. ${err}`
      })
    });
  });

  app.get('/api/allBookmarks', (req, res) => {
    Bookmark.find({}, ( err, bookmarks ) => {
      if(err) res.send(500, err);      
      res.send(200, bookmarks);
    });
  });
  
  app.put('/api/saveBookmark', (req, res) => {
    const newBookmark = new Bookmark(req.body);
    newBookmark.save((err, createdBookmark) => {
      if (err) res.send(500, err);      
      res.send(200, createdBookmark);
    });    
  });
}