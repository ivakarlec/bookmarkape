config = require("./config.json");

module.exports = {
    getDBConnection: () => {
        return `mongodb://${config.uname}:${config.pwd}@${config.mlabUri}`;
    }
}